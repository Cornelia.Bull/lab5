package cellular;

import java.util.ArrayList;

import cellular.gui.CellAutomataGUI;
import datastructure.Grid;
import datastructure.IGrid;

public class Main {

	public static void main(String[] args) {

		ICellAutomaton ca = new LangtonsAnt(50, 50, "RRLLLRLLLRRR");
		// ICellAutomaton ca = new SeedsAutomaton(50, 50);
		// ICellAutomaton ca = new GameOfLife(50, 50);

		CellAutomataGUI.run(ca);
	}

	IGrid<Integer> myGrid = new Grid<Integer>(20, 20, null);
}
